# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the kwin package.
#
# SPDX-FileCopyrightText: 2023, 2024 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-09 00:39+0000\n"
"PO-Revision-Date: 2024-03-15 01:17+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.0\n"

#: ui/main.qml:32
#, kde-format
msgid ""
"Some legacy X11 apps require the ability to read keystrokes typed in other "
"apps for certain features, such as handling global keyboard shortcuts. This "
"is allowed by default. However other features may require the ability to "
"read all keys, and this is disabled by default for security reasons. If you "
"need to use such apps, you can choose your preferred balance of security and "
"functionality here."
msgstr ""

#: ui/main.qml:48
#, kde-format
msgid "Allow legacy X11 apps to read keystrokes typed in all apps:"
msgstr ""
"Permitir que les aplicaciones heredaes de X11 llean les pulsaciones de "
"tecles feches en toles aplicaciones:"

#: ui/main.qml:49
#, kde-format
msgid "Never"
msgstr "Enxamás"

#: ui/main.qml:55
#, kde-format
msgid "Only Meta, Control, Alt and Shift keys"
msgstr ""

#: ui/main.qml:61
#, kde-format
msgid ""
"As above, plus any key typed while the Control, Alt, or Meta keys are pressed"
msgstr ""

#: ui/main.qml:68
#, kde-format
msgid "Always"
msgstr ""

#: ui/main.qml:78
#, kde-format
msgid "Additionally include mouse buttons"
msgstr ""

#: ui/main.qml:89
#, kde-format
msgid ""
"Note that using this setting will reduce system security to that of the X11 "
"session by permitting malicious software to steal passwords and spy on the "
"text that you type. Make sure you understand and accept this risk."
msgstr ""
"Decátate qu'usar esta opción va amenorgar la seguranza del sistema a la de "
"la sesión X11 permitiendo que'l software malicioso robe contraseñes y "
"escluque'l testu qu'escribas. Asegúrate de qu'entiendes y aceptes esti "
"riesgu."
